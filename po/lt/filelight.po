# Lithuanian translations for filelight package.
# Copyright (C) 2010 KDE
# This file is distributed under the same license as the filelight package.
#
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Tomas Straupis <tomasstraupis@gmail.com>, 2011.
# Liudas Alisauskas <liudas@akmc.lt>, 2013.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: filelight\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-16 00:54+0000\n"
"PO-Revision-Date: 2015-12-29 20:56+0200\n"
"Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>\n"
"Language-Team: lt <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Lokalize 1.5\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrius Štikonas"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrius@stikonas.eu"

#. i18n: ectx: Menu (file)
#: misc/filelightui.rc:4
#, kde-format
msgid "&Scan"
msgstr "&Skenuoti"

#. i18n: ectx: Menu (view)
#: misc/filelightui.rc:18
#, kde-format
msgid "&View"
msgstr "&Rodymas"

#. i18n: ectx: ToolBar (mainToolBar)
#: misc/filelightui.rc:24
#, kde-format
msgid "Main Toolbar"
msgstr "Pagrindinė įrankinė"

#. i18n: ectx: ToolBar (locationToolBar)
#: misc/filelightui.rc:33
#, kde-format
msgid "Location Toolbar"
msgstr "Vietos įrankinė"

#. i18n: ectx: ToolBar (viewToolBar)
#: misc/filelightui.rc:38
#, kde-format
msgid "View Toolbar"
msgstr "Rodymo įrankinė"

#: src/Config.cpp:58
#, kde-format
msgid "Select path to ignore"
msgstr ""

#: src/Config.cpp:71
#, fuzzy, kde-format
#| msgid "That folder is already set to be excluded from scans"
msgid "That folder is already set to be excluded from scans."
msgstr "Jau nurodyta, kad šio aplanko nereikia skenuoti"

#: src/Config.cpp:71
#, kde-format
msgid "Folder already ignored"
msgstr ""

#: src/contextMenuContext.cpp:63
#, kde-format
msgid ""
"<qt>The folder at <i>'%1'</i> will be <b>recursively</b> and <b>permanently</"
"b> deleted.</qt>"
msgstr ""
"<qt>Aplankas <i>„%1“</i> bus pilnai <b>rekursyviai</b> ir <b>galutinai</b> "
"ištrintas.</qt>"

#: src/contextMenuContext.cpp:64
#, kde-format
msgid "<qt><i>'%1'</i> will be <b>permanently</b> deleted.</qt>"
msgstr "<qt><i>„%1“</i> bus <b>galutinai</b> ištrintas.</qt>"

#: src/contextMenuContext.cpp:65
#, kde-format
msgid "&Delete"
msgstr "&Trinti"

#: src/contextMenuContext.cpp:92
#, kde-format
msgid "Error while deleting"
msgstr "Klaida trinant"

#: src/historyAction.cpp:64
#, kde-format
msgctxt "Go to the last path viewed"
msgid "Back"
msgstr "Atgal"

#: src/historyAction.cpp:68
#, kde-format
msgctxt "Go to forward in the history of paths viewed"
msgid "Forward"
msgstr "Pirmyn"

#: src/main.cpp:59
#, kde-format
msgid "Graphical disk-usage information"
msgstr "Grafinė disko naudojimo informacija"

#: src/main.cpp:61
#, fuzzy, kde-format
#| msgid ""
#| "(C) 2006 Max Howell\n"
#| "        (C) 2008-2013 Martin Sandsmark"
msgid ""
"(C) 2006 Max Howell\n"
"(C) 2008-2014 Martin Sandsmark\n"
"(C) 2017-2022 Harald Sitter"
msgstr ""
"(C) 2006 Max Howell\n"
"        (C) 2008-2013 Martin Sandsmark"

#: src/main.cpp:66
#, kde-format
msgid "Martin Sandsmark"
msgstr "Martin Sandsmark"

#: src/main.cpp:66
#, kde-format
msgid "Maintainer"
msgstr "Prižiūrėtojas"

#: src/main.cpp:67
#, kde-format
msgid "Harald Sitter"
msgstr ""

#: src/main.cpp:67
#, kde-format
msgid "QtQuick Port"
msgstr ""

#: src/main.cpp:68
#, kde-format
msgid "Max Howell"
msgstr "Max Howell"

#: src/main.cpp:68
#, kde-format
msgid "Original author"
msgstr "Pradinis autorius"

#: src/main.cpp:69
#, kde-format
msgid "Lukas Appelhans"
msgstr "Lukas Appelhans"

#: src/main.cpp:69
#, kde-format
msgid "Help and support"
msgstr "Pagalba ir palaikymas"

#: src/main.cpp:70
#, kde-format
msgid "Steffen Gerlach"
msgstr "Steffen Gerlach"

#: src/main.cpp:70
#, kde-format
msgid "Inspiration"
msgstr "Įkvėpimas"

#: src/main.cpp:71
#, kde-format
msgid "Mike Diehl"
msgstr "Mike Diehl"

#: src/main.cpp:71
#, kde-format
msgid "Original documentation"
msgstr "Pradinė dokumentacija"

#: src/main.cpp:72
#, kde-format
msgid "Sune Vuorela"
msgstr "Sune Vuorela"

#: src/main.cpp:72 src/main.cpp:73
#, kde-format
msgid "Icon"
msgstr "Ženkliukas"

#: src/main.cpp:73
#, kde-format
msgid "Nuno Pinheiro"
msgstr "Nuno Pinheiro"

#: src/main.cpp:80
#, kde-format
msgid "Path or URL to scan"
msgstr ""

#: src/main.cpp:80
#, kde-format
msgid "[url]"
msgstr ""

#: src/mainContext.cpp:163
#, kde-format
msgid "Select Folder to Scan"
msgstr "Parinkite aplanką skenavimui"

#: src/mainContext.cpp:227
#, kde-format
msgid "The entered URL cannot be parsed; it is invalid."
msgstr "Įvestas URL negali būti panaudotas, nes jis neteisingas."

#: src/mainContext.cpp:229
#, kde-format
msgid "Filelight only accepts absolute paths, eg. /%1"
msgstr "Filelight priima tik absoliučius kelius, pavyzdžiui /%1"

#: src/mainContext.cpp:231
#, kde-format
msgid "Folder not found: %1"
msgstr "Nerastas aplankas: %1"

#: src/mainContext.cpp:233
#, kde-format
msgid ""
"Unable to enter: %1\n"
"You do not have access rights to this location."
msgstr ""
"Nepavyko įeiti į: %1\n"
"Jūs neturite teisės įeiti į šią vietą."

#: src/qml/GlobalMenu.qml:10
#, fuzzy, kde-format
#| msgid "&Scan"
msgctxt "@item:inmenu"
msgid "Scan"
msgstr "&Skenuoti"

#: src/qml/GlobalMenu.qml:21
#, kde-format
msgctxt "@item:inmenu"
msgid "Settings"
msgstr ""

#: src/qml/GlobalMenu.qml:27
#, kde-format
msgctxt "@item:inmenu"
msgid "Help"
msgstr ""

#: src/qml/main.qml:30
#, fuzzy, kde-format
#| msgid "Scan Folder"
msgctxt "@action"
msgid "Scan Folder"
msgstr "Skenuoti aplanką"

#: src/qml/main.qml:37
#, fuzzy, kde-format
#| msgid "Scan &Home Folder"
msgctxt "@action"
msgid "Scan Home Folder"
msgstr "Skenuoti &namų aplanką"

#: src/qml/main.qml:44
#, fuzzy, kde-format
#| msgid "Scan &Root Folder"
msgctxt "@action"
msgid "Scan Root Folder"
msgstr "Skenuoti š&akninį aplanką"

#: src/qml/main.qml:51
#, kde-format
msgctxt "@action"
msgid "Quit"
msgstr ""

#: src/qml/main.qml:59
#, kde-format
msgctxt "@action configure app"
msgid "Configure…"
msgstr ""

#: src/qml/main.qml:64
#, kde-format
msgctxt "@title:window"
msgid "Configure"
msgstr ""

#: src/qml/main.qml:76
#, kde-format
msgctxt "@action"
msgid "Open Handbook"
msgstr ""

#: src/qml/main.qml:84
#, kde-format
msgctxt "@action opens about app page"
msgid "About"
msgstr ""

#: src/qml/main.qml:105
#, fuzzy, kde-format
#| msgid "No files."
msgctxt "@info:status"
msgid "No files."
msgstr "Nėra failų."

#: src/qml/main.qml:106
#, fuzzy, kde-format
#| msgid "1 file"
#| msgid_plural "%1 files"
msgctxt "@info:status"
msgid "1 file"
msgid_plural "%1 files"
msgstr[0] "%1 failas"
msgstr[1] "%1 failai"
msgstr[2] "%1 failų"
msgstr[3] "%1 failas"

#: src/qml/main.qml:176
#, fuzzy, kde-format
#| msgid "Aborting Scan..."
msgctxt "@info:status"
msgid "Aborting Scan..."
msgstr "Nutraukiamas skenavimas..."

#: src/qml/MapPage.qml:29
#, kde-format
msgctxt "@action"
msgid "Go to Overview"
msgstr ""

#: src/qml/MapPage.qml:37
#, kde-format
msgctxt "@action"
msgid "Up"
msgstr ""

#: src/qml/MapPage.qml:46
#, fuzzy, kde-format
#| msgid "Rescan"
msgctxt "@action"
msgid "Rescan"
msgstr "Nuskaityti dar kartą"

#: src/qml/MapPage.qml:55
#, fuzzy, kde-format
#| msgid "Stop"
msgctxt "@action"
msgid "Stop"
msgstr "Stabdyti"

#: src/qml/MapPage.qml:63
#, kde-format
msgctxt "@action"
msgid "Zoom In"
msgstr ""

#: src/qml/MapPage.qml:73
#, kde-format
msgctxt "@action"
msgid "Zoom Out"
msgstr ""

#: src/qml/MapPage.qml:104 src/qml/MapPage.qml:222
#, fuzzy, kde-format
#| msgctxt "Scan/open the path of the selected element"
#| msgid "&Open"
msgctxt "@action Open file or directory from context menu"
msgid "Open"
msgstr "&Atverti"

#: src/qml/MapPage.qml:112 src/qml/MapPage.qml:230
#, fuzzy, kde-format
#| msgid "Open &Terminal Here"
msgctxt "@action"
msgid "Open Terminal Here"
msgstr "Atverti &terminalą čia"

#: src/qml/MapPage.qml:120
#, fuzzy, kde-format
#| msgid "&Center Map Here"
msgctxt "@action focuses the filelight view on a given map segment"
msgid "Center Map Here"
msgstr "&Centruoti žemėlapį čia"

#: src/qml/MapPage.qml:131 src/qml/MapPage.qml:238
#, kde-format
msgctxt "@action"
msgid "Add to Do Not Scan List"
msgstr ""

#: src/qml/MapPage.qml:139
#, fuzzy, kde-format
#| msgid "Rescan"
msgctxt "@action rescan filelight map"
msgid "Rescan"
msgstr "Nuskaityti dar kartą"

#: src/qml/MapPage.qml:146 src/qml/MapPage.qml:245
#, fuzzy, kde-format
#| msgid "&Copy to clipboard"
msgctxt "@action"
msgid "Copy to clipboard"
msgstr "&Kopijuoti į iškarpinę"

#: src/qml/MapPage.qml:153 src/qml/MapPage.qml:252
#, fuzzy, kde-format
#| msgid "&Delete"
msgctxt "@action delete file or folder"
msgid "Delete"
msgstr "&Trinti"

#: src/qml/MapPage.qml:305
#, fuzzy, kde-format
#| msgid "%1 File"
#| msgid_plural "%1 Files"
msgctxt "Tooltip of folder, %1 is number of files"
msgid "%1 File (%2%)"
msgid_plural "%1 Files (%2%)"
msgstr[0] "%1 failas"
msgstr[1] "%1 failai"
msgstr[2] "%1 failų"
msgstr[3] "%1 failas"

#: src/qml/MapPage.qml:307
#, fuzzy, kde-format
#| msgid "%1 File"
#| msgid_plural "%1 Files"
msgctxt "Tooltip of folder, %1 is number of files"
msgid "%1 File"
msgid_plural "%1 Files"
msgstr[0] "%1 failas"
msgstr[1] "%1 failai"
msgstr[2] "%1 failų"
msgstr[3] "%1 failas"

#: src/qml/MapPage.qml:311
#, fuzzy, kde-format
#| msgid ""
#| "\n"
#| "Click to go up to parent directory"
msgctxt "part of tooltip indicating that the item under the mouse is clickable"
msgid "Click to go up to parent directory"
msgstr ""
"\n"
"Spustelėkite, kad eiti į aukštesnį katalogą"

#: src/qml/MapPage.qml:501
#, fuzzy, kde-format
#| msgid "%1 File"
#| msgid_plural "%1 Files"
msgctxt "Scanned number of files and size so far"
msgid "%1 File, %2"
msgid_plural "%1 Files, %2"
msgstr[0] "%1 failas"
msgstr[1] "%1 failai"
msgstr[2] "%1 failų"
msgstr[3] "%1 failas"

#: src/qml/MapPage.qml:510
#, kde-format
msgid "No data available"
msgstr ""

#: src/qml/OverviewPage.qml:15
#, kde-format
msgctxt "@title"
msgid "Overview"
msgstr ""

#: src/qml/OverviewPage.qml:35
#, fuzzy, kde-format
#| msgid "Settings - Filelight"
msgctxt "@title"
msgid "Welcome to Filelight"
msgstr "Nutatymai – Filelight"

#: src/qml/SettingsPage.qml:24
#, fuzzy, kde-format
#| msgid "Scannin&g"
msgctxt "@title"
msgid "Scanning"
msgstr "&Skenuojama"

#: src/qml/SettingsPage.qml:27
#, fuzzy, kde-format
#| msgid "&Appearance"
msgctxt "@title"
msgid "Appearance"
msgstr "&Išvaizda"

#: src/qml/SettingsPageAppearance.qml:16
#, fuzzy, kde-format
#| msgid "Color scheme"
msgctxt "@title:group"
msgid "Color Scheme"
msgstr "Spalvų schema"

#: src/qml/SettingsPageAppearance.qml:25
#, fuzzy, kde-format
#| msgid "Rainbow"
msgctxt "@option:radio a color scheme variant"
msgid "Rainbow"
msgstr "Vaivorykštė"

#: src/qml/SettingsPageAppearance.qml:34
#, fuzzy, kde-format
#| msgid "System Colors"
msgctxt "@option:radio a color scheme variant"
msgid "System colors"
msgstr "Sistemos spalvos"

#: src/qml/SettingsPageAppearance.qml:43
#, fuzzy, kde-format
#| msgid "High Contrast"
msgctxt "@option:radio a color scheme variant"
msgid "High contrast"
msgstr "Didelis kontrastas"

#: src/qml/SettingsPageAppearance.qml:52
#, fuzzy, kde-format
#| msgid "Co&ntrast"
msgctxt "@label:slider"
msgid "Contrast"
msgstr "Ko&ntrastas"

#: src/qml/SettingsPageAppearance.qml:69
#, fuzzy, kde-format
#| msgid "Show small files"
msgctxt "@checkbox"
msgid "Show small files"
msgstr "Rodyti mažus failus"

#: src/qml/SettingsPageScanning.qml:23
#, fuzzy, kde-format
#| msgid "Do &not scan these folders:"
msgctxt "@label"
msgid "Do not scan these folders:"
msgstr "&Neskenuoti šių aplankų:"

#: src/qml/SettingsPageScanning.qml:48
#, fuzzy, kde-format
#| msgid "R&emove"
msgctxt "@action:button remove list entry"
msgid "Remove"
msgstr "P&ašalinti"

#: src/qml/SettingsPageScanning.qml:63
#, kde-format
msgctxt "@action:button remove list entry"
msgid "Add…"
msgstr ""

#: src/qml/SettingsPageScanning.qml:75
#, fuzzy, kde-format
#| msgid "Scan across filesystem &boundaries"
msgctxt "@checkbox"
msgid "Scan across filesystem boundaries"
msgstr "Skanuoti &už failų sistemos ribų"

#: src/qml/SettingsPageScanning.qml:86
#, fuzzy, kde-format
#| msgid "Exclude remote files&ystems"
msgctxt "@checkbox"
msgid "Exclude remote filesystems"
msgstr "Išskyrus &nutolusias sistemas"

#: src/radialMap/map.cpp:57
#, fuzzy, kde-format
#| msgid "1 file, with an average size of %2"
#| msgid_plural "%1 files, with an average size of %2"
msgid ""
"\n"
"%1 file, with an average size of %2"
msgid_plural ""
"\n"
"%1 files, with an average size of %2"
msgstr[0] "%1 failas, vidutinis dydis %2"
msgstr[1] "%1 failai, vidutinis dydis %2"
msgstr[2] "%1 failų, vidutinis dydis %2"
msgstr[3] "%1 failas, vidutinis dydis %2"

#~ msgid "Settings - Filelight"
#~ msgstr "Nutatymai – Filelight"

#~ msgid "&Add..."
#~ msgstr "&Pridėti..."

#~ msgid ""
#~ "Prevents scanning of filesystems that are not on this computer, e.g. NFS "
#~ "or Samba mounts."
#~ msgstr ""
#~ "Neleidžia skenuoti failų sistemų, esančių kituose kompiuteriuose, "
#~ "pavyzdžiui NFS arba Samba jungčių."

#~ msgid ""
#~ "Allows scans to enter directories that are part of other filesystems. For "
#~ "example, when unchecked, this will usually prevent the contents of <b>/"
#~ "mnt</b> from being scanned if you scan <b>/</b>."
#~ msgstr ""
#~ "Leidžia skenavimui įeiti į aplankus, esančius kitose failų sistemose. "
#~ "Pavyzdžiui, išjungus šią parinktį, skenuojant <b>/</b> dažniausiai nebus "
#~ "skenuojamas <b>/mnt</b>."

#~ msgid "Here you can vary the contrast of the filemap in realtime."
#~ msgstr "Čia galite keisti failų žemėlapio kontrastą realiam laike."

#~ msgid ""
#~ "Some files are too small to be rendered on the filemap. Selecting this "
#~ "option makes these files visible by merging them all into a single "
#~ "\"multi-segment\"."
#~ msgstr ""
#~ "Kai kurie failai per maži, kad matytųsi žemėlapyje. Įjungus šią parinktį, "
#~ "šie failai bus sujungti į vieną „multisegmentą“, kad būtų matomi."

#~ msgid ""
#~ "Anti-aliasing the filemap makes it clearer and prettier, unfortunately it "
#~ "also makes rendering very slow."
#~ msgstr ""
#~ "Glodinimas padaro žemėlapį švaresniu ir gražesniu. Deja jį ir panaudoti "
#~ "užtrunka ilgiau."

#~ msgid "&Use anti-aliasing"
#~ msgstr "Naudoti &glodinimą"

#~ msgid ""
#~ "The font size of exploded labels can be varied relative to the depth of "
#~ "the directories they represent. This helps you spot the important labels "
#~ "more easily. Set a sensible minimum font size."
#~ msgstr ""
#~ "Iššokančių antraščių šriftas ir jo dydis gali kisti, priklausomai "
#~ "vaizduojamų aplankų gylio. Tokiu būdu galėsite lengviau pastebėti "
#~ "svarbesnes antraštes. Nurodykite protingą minimalų šrifto dydį."

#~ msgid "Var&y label font sizes"
#~ msgstr "&Kintami antraščių šriftų dydžiai"

#~ msgid "The smallest font size Filelight can use to render labels."
#~ msgstr "Mažiausias šrifto dydis, kurį Filelight gali naudoti antraštėms."

#~ msgid "Minimum font si&ze:"
#~ msgstr "Minimalus šrifto &dydis:"

#~ msgid "Open &File Manager Here"
#~ msgstr "Atverti &failų tvarkyklę čia"

#, fuzzy
#~| msgid "Rescan"
#~ msgid "&Rescan"
#~ msgstr "Nuskaityti dar kartą"

#~ msgctxt "We messed up, the user needs to initiate a rescan."
#~ msgid ""
#~ "Internal representation is invalid,\n"
#~ "please rescan."
#~ msgstr ""
#~ "Neteisingas vidinis atvaizdavimas,\n"
#~ "Prašome perskenuoti."

#, fuzzy
#~| msgid "Scan completed, generating map..."
#~ msgctxt "@info:status"
#~ msgid "Scan completed, generating map..."
#~ msgstr "Skenavimas baigtas, kuriamas žemėlapis..."

#~ msgid "Go"
#~ msgstr "Eiti"

#~ msgid "Location Bar"
#~ msgstr "Vietos juosta"

#~ msgid "&Recent Scans"
#~ msgstr "&Paskutiniai skenavimai"

#~ msgid "Scanning: %1"
#~ msgstr "Skenuojamas: %1"

#~ msgid "Scan failed: %1"
#~ msgstr "Skenavimas nepavyko: %1"

#, fuzzy
#~| msgctxt "Percent used disk space on the partition"
#~| msgid "</b> (%1% Used)"
#~ msgctxt "Percent used disk space on the partition"
#~ msgid "<b>%1</b><br/>%2% Used"
#~ msgstr "</b> (%1% naudojama)"

#, fuzzy
#~| msgctxt "Percent used disk space on the partition"
#~| msgid "</b> (%1% Used)"
#~ msgctxt "Percent used disk space on the partition"
#~ msgid "<b>%1: %2</b><br/>%3% Used"
#~ msgstr "</b> (%1% naudojama)"

#~ msgid "Prevents Filelight from scanning removable media (eg. CD-ROMs)."
#~ msgstr ""
#~ "Neleidžia Filelight skenuoti išimamų laikmenų (pvz. kompaktinių diskų)."

#~ msgid "E&xclude removable media"
#~ msgstr "Išskyrus &atjungiamas laikmenas"

#~ msgid "%1 File"
#~ msgid_plural "%1 Files"
#~ msgstr[0] "%1 failas"
#~ msgstr[1] "%1 failai"
#~ msgstr[2] "%1 failų"
#~ msgstr[3] "%1 failas"

#~ msgid "File: %1"
#~ msgid_plural "Files: %1"
#~ msgstr[0] "Failas: %1"
#~ msgstr[1] "Failai: %1"
#~ msgstr[2] "Failai: %1"
#~ msgstr[3] "Failai: %1"

#~ msgctxt "Free space on the disks/partitions"
#~ msgid "Free"
#~ msgstr "Laisva"

#~ msgctxt "Used space on the disks/partitions"
#~ msgid "Used"
#~ msgstr "Naudojama"

#, fuzzy
#~| msgid ""
#~| "Unable to load the Filelight Part.\n"
#~| "Please make sure Filelight was correctly installed."
#~ msgid ""
#~ "Unable to locate the Filelight Part.\n"
#~ "Please make sure Filelight was correctly installed."
#~ msgstr ""
#~ "Nepavyko įkelti Filelight dalies.\n"
#~ "Įsitikinkite, kad Filelight įdiegtas teisingai."

#~ msgid ""
#~ "Unable to load the Filelight Part.\n"
#~ "Please make sure Filelight was correctly installed."
#~ msgstr ""
#~ "Nepavyko įkelti Filelight dalies.\n"
#~ "Įsitikinkite, kad Filelight įdiegtas teisingai."

#, fuzzy
#~| msgid ""
#~| "Unable to load the Filelight Part.\n"
#~| "Please make sure Filelight was correctly installed."
#~ msgid ""
#~ "Unable to create Filelight part widget.\n"
#~ "Please ensure that Filelight is correctly installed."
#~ msgstr ""
#~ "Nepavyko įkelti Filelight dalies.\n"
#~ "Įsitikinkite, kad Filelight įdiegtas teisingai."

#~ msgid "Displays file usage in an easy to understand way."
#~ msgstr "Paprastai ir suprantamai parodo failų naudojimą."

#, fuzzy
#~| msgid ""
#~| "(c) 2002-2004 Max Howell\n"
#~| "                (c) 2008-2013 Martin T. Sandsmark"
#~ msgid ""
#~ "(c) 2002-2004 Max Howell\n"
#~ "(c) 2008-2014 Martin T. Sandsmark"
#~ msgstr ""
#~ "(c) 2002-2004 Max Howell\n"
#~ "                (c) 2008-2013 Martin T. Sandsmark"

#~ msgid "Configure Filelight..."
#~ msgstr "Konfigūruoti Filelight..."

#~ msgctxt "Path in the file system to scan"
#~ msgid "+[path]"
#~ msgstr "+[kelias]"

#~ msgid "Scan 'path'"
#~ msgstr "Skenuoti „kelią“"

#~ msgid "Unable to create part widget."
#~ msgstr "Nepavyko sukurti dalies valdiklio."
